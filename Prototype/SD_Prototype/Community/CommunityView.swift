//
//  CommunityView.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/3/31.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct CommunityView: View {
    
    
    var body: some View {
        NavigationView {
            VStack {
                List {
                    NavigationLink(destination: PlayersView()) {
                        ListElement(iconName: "person.crop.circle", elementName: "Players", color: .blue)
                    }
                    
                    NavigationLink(destination: ClubsView()) {
                        ListElement(iconName: "suit.club.fill", elementName: "Clubs", color: .orange)
                    }
                    
                    NavigationLink(destination: DiscussionsView()) {
                        ListElement(iconName: "bubble.left.and.bubble.right.fill", elementName: "Discussions", color: .purple)
                    }
                    
                    NavigationLink(destination: ResourcesView()) {
                        ListElement(iconName: "book.fill", elementName: "Resources", color: .red)
                    }
                }
            }
            .navigationBarTitle(Text("Community"))
            .padding(.leading, -20)
            .padding(.trailing, 10)
            
            Spacer()
        }
    }
}

struct CommunityView_Previews: PreviewProvider {
    static var previews: some View {
        CommunityView()
    }
}
