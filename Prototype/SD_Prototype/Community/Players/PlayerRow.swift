//
//  PlayerRow.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct PlayerRow: View {
    var player: Player
    
    var body: some View {
        HStack {
            Image(player.imageName)
                .renderingMode(.original)
                .resizable()
                .frame(width: 50, height: 50)
                .cornerRadius(8)
                .shadow(radius: 6)
            Text(player.username)
                .font(.system(size: 20, weight: .bold))
                .offset(x: 10)
                .foregroundColor(.primary)
            Spacer()
        }
        .padding(.horizontal)
        .padding(.vertical, 10.0)
    }
}

struct PlayerRow_Previews: PreviewProvider {
    static var previews: some View {
        PlayerRow(player: player_data[0])
    }
}
