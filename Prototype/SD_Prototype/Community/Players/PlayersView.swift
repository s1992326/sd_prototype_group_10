//
//  PlayersView.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct PlayersView: View {
    @State private var input: String = ""
    
    var body: some View {
        VStack {
            HStack {
                VStack {
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.secondary)
                        TextField("Search by Player Names", text: $input)
                    }
                    .padding()
                    .frame(width: 260, height: 48)
                    .overlay(
                        RoundedRectangle(cornerRadius: 12)
                            .stroke(Color.gray, lineWidth: 2)
                            .shadow(radius: 8)
                    )
                }
                .offset(x: -2, y: 0)
                
                navigateButton(destinationView: SearchResult(), title: "Search")
                    .padding(-35.0)
                    .offset(x: 2, y: 11)
            }
            .padding()
            .offset(y: 5)
            
            Spacer()
            
            List(player_data) { player in
                NavigationLink(destination: PlayerDetails(player: player)) {
                    PlayerRow(player: player)
                        .padding(.leading, 5)
                }
            }
            .padding(.leading, -20)
            
            Spacer()
            
        }
        .navigationBarTitle(Text("Players"), displayMode: .inline)
        .navigationBarBackButtonHidden(false)
        
    }
}

struct PlayersView_Previews: PreviewProvider {
    static var previews: some View {
        PlayersView()
    }
}
