//
//  PlayerCollection.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct PlayerCollection: View {
    var playerlist: [Int]
    
    var body: some View {
        VStack {
            ForEach(player_data) { player in
                if self.playerlist.contains(player.id) {
                    NavigationLink(destination: PlayerDetails(player: player)
                    ) {
                        PlayerRow(player: player)
                    }
                }
            }
            Spacer()
        }
        .padding(.vertical, 10.0)
    }
}

struct PlayerCollection_Previews: PreviewProvider {
    static var previews: some View {
        PlayerCollection(playerlist: club_data[0].players)
    }
}
