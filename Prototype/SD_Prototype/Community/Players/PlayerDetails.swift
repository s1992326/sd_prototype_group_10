//
//  PlayerDetails.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct PlayerDetails: View {
    var player: Player
    
    var body: some View {
        VStack {
            ScrollView {
                Image(player.backgroundImage)
                    .resizable()
                    .scaledToFill()
                    .frame(height: 300)
                    .clipped()
                    .listRowInsets(EdgeInsets())
                    .padding(.bottom, -160)
                
                CircleImage(image: Image(player.imageName).resizable())
                    .padding(.bottom)
                
                Text(player.username)
                    .font(.largeTitle)
                    .fontWeight(.heavy)
                    .padding(.bottom)
                
                HStack {
                    VStack(alignment: .leading) {
                        Text("Description")
                            .font(.system(size: 24))
                            .fontWeight(.medium)
                            .multilineTextAlignment(.leading)
                            .padding(.leading, 80)
                            .padding(.top, 10)
                        
                        Text("This is the description of this player.")
                            .lineLimit(nil)
                            .padding(.leading, 80)
                            .padding(.top, 15)
                    }
                    Spacer()
                }
                .padding(.bottom, 20)
                
                Divider()
                
                VStack {
                    Text("Favorites")
                        .font(.system(size: 24))
                        .fontWeight(.medium)
                        .multilineTextAlignment(.leading)
                        .padding(.top, 10)
                        .offset(x: -135)
                    
                    GameCollection(gamelist: player.favoriteGame)
                        .environmentObject(GameList())
                        .padding(.leading, 67)
                }
                
                Divider()
                
                VStack {
                    Text("Owned")
                        .font(.system(size: 24))
                        .fontWeight(.medium)
                        .multilineTextAlignment(.leading)
                        .padding(.top, 10)
                        .offset(x: -145)
                    
                    GameCollection(gamelist: player.ownedGame)
                        .environmentObject(GameList())
                        .padding(.leading, 67)
                        .padding(.bottom, 20)
                }
                
                Divider()
                
                VStack {
                    Text("Location")
                        .font(.system(size: 24))
                        .fontWeight(.medium)
                        .multilineTextAlignment(.leading)
                        .padding(.top, 10)
                        .padding(.bottom, 10)
                        .offset(x: -135)
                    
                    MapView(coordinate: player.locationCoordinate)
                        .frame(height: 320)
                }
                
            }
        }
        .navigationBarTitle(player.username)
    }
}

struct PlayerDetails_Previews: PreviewProvider {
    static var previews: some View {
        PlayerDetails(player: player_data[0])
    }
}
