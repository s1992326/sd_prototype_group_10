//
//  ClubRow.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct ClubRow: View {
    var club: Club
    
    var body: some View {
        HStack {
            Image(club.imageName)
                .renderingMode(.original)
                .resizable()
                .frame(width: 50, height: 50)
                .cornerRadius(8)
            Text(club.name)
                .font(.system(size: 20, weight: .bold))
                .offset(x: 10)
                .foregroundColor(.primary)
            Spacer()
        }
        .padding(.horizontal)
        .padding(.vertical, 10.0)
    }
}

struct ClubRow_Previews: PreviewProvider {
    static var previews: some View {
        ClubRow(club: club_data[0])
    }
}
