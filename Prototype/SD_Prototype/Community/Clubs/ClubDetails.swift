//
//  ClubDetails.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct ClubDetails: View {
    var club: Club
    
    var body: some View {
        VStack {
            ScrollView {
                Image(club.backgroundName)
                    .resizable()
                    .scaledToFill()
                    .frame(height: 250)
                    .clipped()
                    .listRowInsets(EdgeInsets())
                    .padding(.bottom, 15)
                
                VStack {
                    HStack {
                        Text(club.name)
                            .font(.system(size: 28))
                            .fontWeight(.bold)
                        
                        Spacer()
                    }
                    .padding(.bottom, 20.0)
                    .offset(x: 35, y: 0)
                }
                
                VStack(alignment: .leading) {
                    Text("Description")
                        .font(.system(size: 24))
                        .fontWeight(.medium)
                        .padding(.leading, 10)
                        .padding(.top, 10)
                    
                    Text(club.description)
                        .lineLimit(nil)
                        .padding(.leading, 10)
                        .padding(.top, 15)
                }
                .offset(x: -72, y: 0)
                .padding(.bottom, 20.0)
                
                Divider()
                
                VStack {
                    Text("Players")
                        .font(.system(size: 24))
                        .fontWeight(.medium)
                        .multilineTextAlignment(.leading)
                        .padding(.top, 10)
                        .offset(x: -145)
                    
                    PlayerCollection(playerlist: club.players)
                        .padding(.leading, 25)
                        .padding(.bottom, 20)
                }
            }
        }
        .navigationBarTitle(Text(club.name), displayMode: .inline)
    }
}

struct ClubDetails_Previews: PreviewProvider {
    static var previews: some View {
        ClubDetails(club: club_data[0])
    }
}
