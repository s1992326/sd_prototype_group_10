//
//  ClubsView.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct ClubsView: View {
    @State private var input: String = ""
    
    var body: some View {
        VStack {
            HStack {
                VStack {
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.secondary)
                        TextField("Search by Club Names", text: $input)
                    }
                    .padding()
                    .frame(width: 260, height: 48)
                    .overlay(
                        RoundedRectangle(cornerRadius: 12)
                            .stroke(Color.gray, lineWidth: 2)
                            .shadow(radius: 8)
                    )
                }
                .offset(x: -2, y: 0)
                
                navigateButton(destinationView: SearchResult(), title: "Search")
                    .padding(-35.0)
                    .offset(x: 2, y: 11)
            }
            .padding()
            .offset(y: 5)
            
            Spacer()
            
            List(club_data) { club in
                NavigationLink(destination: ClubDetails(club: club)) {
                    ClubRow(club: club)
                        .padding(.leading, 5)
                }
            }
            .padding(.leading, -20)
            
            Spacer()
            
        }
        .navigationBarTitle(Text("Clubs"), displayMode: .inline)
        .navigationBarBackButtonHidden(false)
        
    }
}

struct ClubsView_Previews: PreviewProvider {
    static var previews: some View {
        ClubsView()
    }
}

