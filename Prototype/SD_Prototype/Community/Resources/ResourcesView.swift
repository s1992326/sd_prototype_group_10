//
//  ResourcesView.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct ResourcesView: View {
    @State private var input: String = ""
    
    var body: some View {
        VStack {
            HStack {
                VStack {
                    HStack {
                        Image(systemName: "magnifyingglass")
                            .foregroundColor(.secondary)
                        TextField("Search by Website Names", text: $input)
                    }
                    .padding()
                    .frame(width: 260, height: 48)
                    .overlay(
                        RoundedRectangle(cornerRadius: 12)
                            .stroke(Color.gray, lineWidth: 2)
                            .shadow(radius: 8)
                    )
                }
                .offset(x: 17, y: 0)
                
                navigateButton(destinationView: SearchResult(), title: "Search")
                    .padding(-30.0)
                    .offset(x: 17, y: 6)
            }
            .padding()
            .offset(y: 10)
            
            List(website_data) { website in
                NavigationLink(destination: WebBrowser(the_url: website.the_url)) {
                    WebsiteRow(website: website)
                        .padding(.leading, 20)
                }
            }
            .navigationBarTitle(Text("Websites"))
            .padding(.leading, -20)
            .padding(.top, 10)
            
            Spacer()
        }
        .navigationBarTitle(Text("Resources"), displayMode: .inline)
        .padding(.leading, -20)
        .padding(.trailing, 10)
    }
}

struct ResourcesView_Previews: PreviewProvider {
    static var previews: some View {
        ResourcesView()
    }
}
