//
//  WebsiteRow.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct WebsiteRow: View {
    var website: Website
    
    var body: some View {
        VStack(spacing: 20) {
            HStack {
                Image(website.imageName)
                    .resizable()
                    .frame(width: 60, height: 60)
                    .cornerRadius(8)
                    .shadow(radius: 6)
                Text(website.name)
                    .font(.system(size: 24, weight: .bold))
                    .fontWeight(.bold)
                    .offset(x: 20)
                Spacer()
            }
            .padding(.leading, 18.0)
            .padding(.bottom, 10)
            
            HStack {
                Text(website.description)
                    .font(.body)
                Spacer()
            }
            .padding(.leading, 20.0)
            .padding(.top, -10)
        }
        .padding(.leading, 10)
        .padding(.bottom, 10)
        .padding(.top, 15)
    }
}

struct WebsiteRow_Previews: PreviewProvider {
    static var previews: some View {
        WebsiteRow(website: website_data[0])
    }
}
