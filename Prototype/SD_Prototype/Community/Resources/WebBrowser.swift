//
//  WebBrowser.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI
import WebKit

struct WebBrowser: View {
    var the_url: String
    
    var body: some View {
        VStack {
            WebView(request: URLRequest(url: URL(string: the_url)!))
        }
    }
}

struct WebView: UIViewRepresentable {
    var request : URLRequest
    
    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        uiView.load(request)
    }
}

struct WebBrower_Previews: PreviewProvider {
    static var previews: some View {
        WebBrowser(the_url: "https://www.google.com")
    }
}
