//
//  DiscussionsDetails.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct DiscussionsDetails: View {
    var forum: Forum
    
    var body: some View {
        VStack {
            ScrollView {
                Image(forum.backgroundName)
                    .resizable()
                    .scaledToFill()
                    .frame(height: 250)
                    .clipped()
                    .listRowInsets(EdgeInsets())
                    .padding(.bottom, 15)
                
                VStack {
                    HStack {
                        Text(forum.name)
                            .font(.system(size: 28))
                            .fontWeight(.bold)
                        
                        Spacer()
                    }
                    .padding(.bottom, 20.0)
                    .offset(x: 35, y: 0)
                }
                
                VStack(alignment: .leading) {
                    Text("Description")
                        .font(.system(size: 24))
                        .fontWeight(.medium)
                        .padding(.leading, 10)
                        .padding(.top, 10)
                    
                    Text(forum.description)
                        .lineLimit(nil)
                        .padding(.leading, 10)
                        .padding(.top, 15)
                }
                .offset(x: -65, y: 0)
                .padding(.bottom, 20.0)
                
                Divider()
                
                VStack {
                    Text("Posts")
                        .font(.system(size: 24))
                        .fontWeight(.medium)
                        .multilineTextAlignment(.leading)
                        .padding(.top, 10)
                        .padding(.bottom, 15)
                        .offset(x: -157)
                    
                    VStack {
                        ForEach(post_data) { post in
                            if self.forum.posts.contains(post.id) {
                                NavigationLink(destination: ReviewDetails(post: post)
                                ) {
                                    PostRow(post: post)
                                }
                                Divider()
                            }
                        }
                        Spacer()
                    }
                    .padding(.vertical, 10.0)
                    .padding(.leading, 20)
                }
                
                navigateButton(destinationView: DiscussionsDetails(forum: forum), title: "Add a post")
                    .padding(.top, 5)
            }
        }
        .navigationBarTitle(Text(forum.name), displayMode: .inline)
    }
}

struct DiscussionsDetails_Previews: PreviewProvider {
    static var previews: some View {
        DiscussionsDetails(forum: forum_data[0])
    }
}
