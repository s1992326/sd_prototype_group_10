//
//  PostRow.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct PostRow: View {
    var post: Post
    
    var body: some View {
        VStack{
            HStack{
                Text(post.headline)
                    .font(.system(size: 20))
                    .fontWeight(.bold)
                    .foregroundColor(.primary)
                Spacer()
            }
            .padding(.leading)
            .padding(.bottom, -5)
            
            HStack {
                Image(post.icon)
                    .renderingMode(.original)
                    .resizable()
                    .frame(width: 50, height: 50)
                    .cornerRadius(8)
                    .shadow(radius: 6)
                Text(post.username)
                    .font(.system(size: 18, weight: .bold))
                    .offset(x: 10)
                    .foregroundColor(.primary)
                Spacer()
            }
            .padding(.horizontal)
            .padding(.vertical, 10.0)
        }
    }
}

struct PostRow_Previews: PreviewProvider {
    static var previews: some View {
        PostRow(post: post_data[0])
    }
}
