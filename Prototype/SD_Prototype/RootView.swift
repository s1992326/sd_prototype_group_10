//
//  RootView.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/3/31.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct RootView: View {
    var body: some View {
        TabView {
            CollectionsView()
                .tabItem {
                    Image(systemName: "gamecontroller.fill")
                    Text("Collections")
            }.tag(1)
            
            CommunityView()
                .tabItem {
                    Image(systemName: "ellipses.bubble")
                    Text("Community")
            }.tag(2)
            
            PropertiesView()
                .tabItem {
                    Image(systemName: "cube.box.fill")
                    Text("Properties")
            }.tag(3)
            
            AccountView(player: player_data[0])
                .tabItem {
                    Image(systemName: "person.fill")
                    Text("Account")
            }.tag(4)
        }
    }
}

struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
