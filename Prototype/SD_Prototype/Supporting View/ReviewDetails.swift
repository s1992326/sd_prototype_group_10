//
//  ReviewDetails.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct ReviewDetails: View {
    var post: Post
    
    var body: some View {
        VStack(spacing: 20) {
            HStack {
                Text(post.headline)
                    .font(.system(size: 24, weight: .medium))
                Spacer()
            }
            .padding(.leading, 10.0)
            
            HStack {
                Image(post.icon)
                    .resizable()
                    .frame(width: 50, height: 50)
                    .cornerRadius(8)
                    .shadow(radius: 6)
                Text(post.username)
                    .font(.system(size: 20, weight: .bold))
                    .fontWeight(.bold)
                    .offset(x: 10)
                Spacer()
            }
            .padding(.leading, 10.0)
            
            HStack {
                Text(post.description)
                    .font(.system(size: 18))
                Spacer()
            }
            .padding(.leading, 10.0)
            
            ForEach(post.responds) { respond in
                VStack(spacing: 20) {
                    Divider()
                        .padding(.bottom, -20.0)
                    HStack {
                        Image(respond.icon)
                            .resizable()
                            .frame(width: 50, height: 50)
                            .cornerRadius(8)
                            .shadow(radius: 6)
                        Text(respond.username)
                            .font(.system(size: 18, weight: .bold))
                            .fontWeight(.bold)
                            .offset(x: 10)
                        Spacer()
                    }
                    .padding(.leading, 5.0)
                    
                    HStack {
                        Text(respond.content)
                            .font(.body)
                        Spacer()
                    }
                    .padding(.leading, 5.0)
                    .padding(.top, -10)
                    
                }
                
            }
            .padding(.leading, 30.0)
            .padding(.trailing, -20)
            .padding(.bottom, -5)
            
            Divider()
                .padding(.horizontal, -20.0)
            
            navigateButton(destinationView: ReviewDetails(post: post), title: "Add a comment")
                .padding(.top, 5)
            
            Spacer()
        }
        .padding()
    }
}


struct ReviewtDetails_Previews: PreviewProvider {
    static var previews: some View {
        ReviewDetails(post: post_data[0])
    }
}
