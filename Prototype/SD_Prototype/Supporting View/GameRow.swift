//
//  GameRow.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/3.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct GameRow: View {
    var game: Game
    
    var body: some View {
        HStack {
            Image(game.imageName)
                .renderingMode(.original)
                .resizable()
                .frame(width: 60, height: 60)
                .cornerRadius(8)
                .shadow(radius: 8)
            Text(game.name)
                .font(.system(size: 20, weight: .bold))
                .offset(x: 10)
                .foregroundColor(.primary)
            Spacer()
        }
        .padding(.horizontal)
        .padding(.vertical, 10.0)
    }
}

struct GameRow_Previews: PreviewProvider {
    static var previews: some View {
        GameRow(game: game_data[0])
    }
}
