//
//  NavigationButton.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct navigateButton<Content: View>: View{
    var title: String
    var destinationView: Content
    
    init(destinationView: Content, title: String) {
        self.destinationView = destinationView
        self.title = title
    }
    
    var body: some View{
        NavigationLink(destination: destinationView) {
            ZStack {
                Rectangle()
                    .fill(Color.pink)
                    .cornerRadius(12)
                    .frame(height: 48)
                    .shadow(radius: 8)
                Text(title)
                    .font(.system(size: 22, weight: .bold))
                    .foregroundColor(.white)
            }
            .padding(.horizontal, 35.0)
        }
    }
    
}

struct navigateButton_Previews: PreviewProvider {
    static var previews: some View {
        navigateButton(destinationView: RootView(), title: "main page")
    }
}
