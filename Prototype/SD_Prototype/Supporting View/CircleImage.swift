//
//  CircleImage.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/1.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct CircleImage: View {
    var image: Image
    
    var body: some View {
        image
            .clipShape(Circle())
            .overlay(Circle().stroke(Color.white, lineWidth: 4))
            .shadow(radius: 10)
            .frame(width: 216.0, height: 216.0)
    }
}
