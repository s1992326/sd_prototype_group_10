//
//  PropertyRow.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/3.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct PropertyRow: View {
    var property: Property
    @State var isEdit: Bool = false
    @State private var input: String = ""
    @State private var description: String = ""
    @State private var p_property: Property = property_data[0]
    
    var body: some View {
        VStack {
            HStack {
                Image(p_property.imageName)
                    .resizable()
                    .frame(width: 40, height: 40)
                    .cornerRadius(8)
                    .shadow(radius: 6)
                    .buttonStyle(PlainButtonStyle())
                if self.isEdit{
                    TextField("", text: $input)
                }else{
                    Text(p_property.name)
                    .font(.system(size: 16, weight: .bold))
                    .fontWeight(.bold)
                    .offset(x: 10)
                }
                
                Spacer()
                Button(action: {
                    if !self.isEdit{
                        self.isEdit.toggle()
                        self.input = self.p_property.name
                        self.description = self.p_property.description
                    }
                    
                }){
                        
                    if self.isEdit{
                        Image(systemName: "square.and.pencil")
                            .resizable()
                            .frame(width: 25, height: 25)
                            .padding()
                            .foregroundColor(Color.gray)
                    }else{
                        Image(systemName: "square.and.pencil")
                            .resizable()
                            .frame(width: 25, height: 25)
                            .padding()
                            .foregroundColor(Color.blue)
                    }
                        
                }
                
            }
            .padding(.leading, 18.0)
            
            HStack {
                if self.isEdit{
                    TextField("", text: $description)
                }else{
                    Text(p_property.description)
                       .font(.body)
                       .padding(.leading, 20)
                }
               
                Spacer()
            }
            .padding(.top, 5)
            .padding(.bottom, 10)
            
            if self.isEdit{
                Button(action: {
                   self.isEdit.toggle()
                   self.p_property.name = self.input
                    self.p_property.description = self.description
               }){
                   Text("Save")
                   .font(.system(size: 20, weight: .bold))
                   .padding(.horizontal, 10)
                   .padding(.top, 5)
                   .padding(.bottom, 5)
                   .foregroundColor(.white)
                   .background(Color.pink)
                   
               }
            }
           
        }
        .padding(.leading, 30)
        .padding(.vertical, 5)
    }
}

struct PropertyRow_Previews: PreviewProvider {
    static var previews: some View {
        PropertyRow(property: property_data[0])
    }
}
