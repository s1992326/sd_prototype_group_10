//
//  PropertyCollection.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/3.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct PropertyCollection: View {
    var gamelist: [Int]
    @State private var isAdd: Bool = false
    @State var input: String = ""
    
    var body: some View {
        ScrollView {
            ForEach(game_data) { game in
                
                if self.gamelist.contains(game.id) {
                    
                    PropertyGroup(game: game)
                    
                }
            }
        }
    }
}

struct PropertyCollection_Previews: PreviewProvider {
    static var previews: some View {
        PropertyCollection(gamelist: player_data[0].ownedGame)
            .environmentObject(GameList())
    }
}
