//
//  PropertyGroup.swift
//  SD_Prototype
//
//  Created by 笪雨嘉 on 2020/4/9.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct PropertyGroup: View {
    var game: Game
    @State private var isAdd = false
    @State var name: String = ""
    @State var description: String = ""
    @State var new_property: [Property] = []
    @State var temp_property: Property = property_data[0]
    @State private var is_show_added: Bool = false
    
    var body: some View {
        VStack{
            NavigationLink(destination: GameDetails(game: game)){
                HStack{
                   GameRow(game:game)
                   Spacer()
                   Button(action:{
                       if !self.isAdd{
                           self.isAdd.toggle()
                       }
                       
                   }){
                       if self.isAdd{
                              Image(systemName: "plus")
                              .resizable()
                              .frame(width: 25, height: 25)
                              .padding()
                              .foregroundColor(Color.gray)
                           }else{
                               Image(systemName: "plus")
                               .resizable()
                               .frame(width: 25, height: 25)
                               .padding()
                               .foregroundColor(Color.blue)
                           }
                       
                       }
                   }
            }
               
                
                
                  HStack {
                      PropertyList(game: game)
                      Spacer()
                  }
                  .padding(.leading, -10)
                  .padding(.top, -10)
            
                    if self.is_show_added{
                        ForEach(self.new_property){ prop in
                            HStack{
                                PropertyRow(property: prop)
                                Spacer()
                            }
                            .padding(.leading, -10)
                            .padding(.top, -10)
                        }
                        
                    }
                              
                  if self.isAdd{
                     
                     VStack {
                          
                        TextField("Enter a new property", text: self.$name)
                          .padding()
                          .frame(width: 260, height: 48)
                          .overlay(
                              RoundedRectangle(cornerRadius: 12)
                                  .stroke(Color.gray, lineWidth: 2)
                                  .shadow(radius: 8)
                            )
                        
                        TextField("Description", text: self.$description)
                        .padding()
                        .frame(width: 260, height: 48)
                        .overlay(
                            RoundedRectangle(cornerRadius: 12)
                                .stroke(Color.gray, lineWidth: 2)
                                .shadow(radius: 8)
                          )
                     
                        Button(action: {
                            if self.name != "" && self.description != ""{
                                self.isAdd.toggle()
                                self.temp_property.belongTo = 0
                                self.temp_property.description = self.description
                                self.temp_property.name = self.name
                                self.new_property.append(self.temp_property)
                                self.name = ""
                                self.description = ""
                                self.is_show_added = true
                            }
                              
                          }){
                              Text("Save")
                              .font(.system(size: 20, weight: .bold))
                              .padding(.horizontal, 20)
                              .padding(.top, 10)
                             .padding(.bottom, 10)
                              .foregroundColor(.white)
                              .background(Color.pink)
                              
                          }
                     
                     }
                     .padding()
                  
                }
            
                
            
                
            
            Divider()
            .padding(.bottom, 5)
        }
    }
    
}


struct PropertyGroup_Previews: PreviewProvider {
    static var previews: some View {
        PropertyGroup(game: game_data[0])
    }
}
