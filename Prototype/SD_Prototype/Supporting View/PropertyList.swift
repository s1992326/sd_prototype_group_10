//
//  PropertyList.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/3.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct PropertyList: View {
    var game: Game
    
    var body: some View {
        VStack {
            ForEach(property_data) { property in
                if property.belongTo == self.game.id {
                    PropertyRow(property: property)
                }
            }
        }
        .padding(.vertical, 10.0)
    }
}

struct PropertyList_Previews: PreviewProvider {
    static var previews: some View {
        PropertyList(game: game_data[0])
    }
}
