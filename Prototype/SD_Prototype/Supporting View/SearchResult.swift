//
//  SearchResult.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct SearchResult: View {
    var body: some View {
        Text("This is the search result.")
    }
}

struct SearchResult_Previews: PreviewProvider {
    static var previews: some View {
        SearchResult()
    }
}
