//
//  ListElement.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct ListElement: View {
    var iconName: String
    var elementName: String
    var color: Color
    
    
    var elementImage: some View {
        Image(systemName: iconName)
            .resizable()
            .frame(width: 75, height: 75)
            .padding()
            .foregroundColor(color)
    }
    
    var body: some View {
        HStack {
            elementImage
                .padding(.trailing)
            Text(elementName)
                .font(.system(size: 30, weight: .bold))
            Spacer()
        }
        .padding()
    }
}

struct ListElement_Previews: PreviewProvider {
    static var previews: some View {
        ListElement(iconName: "person.crop.circle", elementName: "Players", color: .pink)
            .previewLayout(.fixed(width: 300, height: 90))
    }
}
