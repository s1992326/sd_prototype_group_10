//
//  PropertiesViewEdit.swift
//  SD_Prototype
//
//  Created by 笪雨嘉 on 2020/4/8.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct PropertiesViewEdit: View {
    @State private var input: String = ""
       var body: some View {
           NavigationView {
               VStack {
                   Spacer()
                   
                   Divider()
                   
                   HStack {
                       VStack {
                           HStack {
                               Image(systemName: "magnifyingglass")
                                   .foregroundColor(.secondary)
                               TextField("Search by Game Names", text: $input)
                           }
                           .padding()
                           .frame(width: 260, height: 48)
                           .overlay(
                               RoundedRectangle(cornerRadius: 12)
                                   .stroke(Color.gray, lineWidth: 2)
                                   .shadow(radius: 8)
                           )
                       }
                       .offset(x: -2, y: 0)
                       
                       navigateButton(destinationView: SearchResult(), title: "Search")
                           .padding(-35.0)
                           .offset(x: 2, y: 11)
                   }
                   .padding()
                   
                   PropertyCollection(gamelist: player_data[0].ownedGame)
                       .environmentObject(GameList())
                       .padding(.leading, 5)
                       .padding(.bottom, 20)
               }
               .navigationBarTitle("Properties")
            
                navigateButton(destinationView: PropertiesView(), title: "Edit")
           }
       }
}

struct PropertiesViewEdit_Previews: PreviewProvider {
    static var previews: some View {
        PropertiesViewEdit()
    }
}
