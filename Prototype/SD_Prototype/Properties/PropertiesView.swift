//
//  PropertiesView.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/3/31.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct PropertiesView: View {
    @State private var input: String = ""
    private var player: Player = player_data[0]
    
    var body: some View {
        NavigationView {
            VStack {
                Spacer()
                
                Divider()
                
                HStack {
                    VStack {
                        HStack {
                            Image(systemName: "magnifyingglass")
                                .foregroundColor(.secondary)
                            TextField("Search by Game Names", text: $input)
                        }
                        .padding()
                        .frame(width: 260, height: 48)
                        .overlay(
                            RoundedRectangle(cornerRadius: 12)
                                .stroke(Color.gray, lineWidth: 2)
                                .shadow(radius: 8)
                        )
                    }
                    .offset(x: -2, y: 0)
                    
                    navigateButton(destinationView: SearchResult(), title: "Search")
                        .padding(-35.0)
                        .offset(x: 2, y: 11)
                }
                .padding()
                
                PropertyCollection(gamelist: player.ownedGame)
                    .environmentObject(GameList())
                    .padding(.leading, 5)
                    .padding(.bottom, 20)
            }
            .navigationBarTitle("Properties")
        }
    }
}

struct PropertiesView_Previews: PreviewProvider {
    static var previews: some View {
        PropertiesView()
    }
}
