//
//  Post.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/1.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct Post: Hashable, Codable, Identifiable {
    var id: Int
    var username: String
    var icon: String
    var headline: String
    var description: String
    var responds: [Respond]
}

struct Respond: Hashable, Codable, Identifiable {
    var id: Int
    var username: String
    var icon: String
    var content: String
}
