//
//  Player.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI
import CoreLocation

struct Player: Hashable, Codable, Identifiable {
    var id: Int
    var username: String
    var imageName: String
    var coordinates: Coordinates
    var backgroundImage: String
    var ownedGame: [Int]
    var favoriteGame: [Int]
    
    var locationCoordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(
            latitude: coordinates.latitude,
            longitude: coordinates.longitude)
    }
}

struct Coordinates: Hashable, Codable {
    var latitude: Double
    var longitude: Double
}
