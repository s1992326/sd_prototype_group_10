//
//  Website.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/2.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct Website: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var imageName: String
    var description: String
    var the_url: String
    var price: [Double]
}
