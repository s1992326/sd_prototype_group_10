//
//  Data.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/3/31.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI
import CoreLocation

let game_data: [Game] = load("GameData.json")
let post_data: [Post] = load("PostData.json")
let player_data: [Player] = load("PlayerData.json")
let club_data: [Club] = load("ClubData.json")
let forum_data: [Forum] = load("ForumData.json")
let website_data: [Website] = load("WebsiteData.json")
let property_data: [Property] = load("PropertyData.json")

func load<T: Decodable>(_ filename: String) -> T {
    let data: Data
    
    guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
        else {
            fatalError("Couldn't find \(filename) in main bundle.")
    }
    
    do {
        data = try Data(contentsOf: file)
    } catch {
        fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
    }
    
    do {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    } catch {
        fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
    }
}

