//
//  Property.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/3.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct Property: Hashable, Codable, Identifiable {
    var name: String
    var id: Int
    var belongTo: Int
    var imageName: String
    var description: String
}
