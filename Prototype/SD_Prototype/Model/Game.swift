//
//  Game.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/3/31.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct Game: Hashable, Codable, Identifiable {
    var name: String
    var category: Category
    var id: Int
    var isFavorite: Bool
    var isOwned: Bool
    var imageName: String
    var backgroundName: String
    var description: String
    var rules: String
    
    enum Category: String, CaseIterable, Codable, Hashable {
        case board = "Board Game"
        case war = "War Game"
        case card = "Card Game"
    }
}
