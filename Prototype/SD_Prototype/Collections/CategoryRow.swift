//
//  CategoryRow.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/1.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct CategoryRow: View {
    @EnvironmentObject private var GameList: GameList
    
    var CategoryName: String
    var items: [Game]
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(self.CategoryName)
                .font(.headline)
                .padding(.leading, 10)
                .padding(.top, 10)
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .top, spacing: 0) {
                    ForEach(self.items) { game in
                        NavigationLink(
                            destination: GameDetails(
                                game: game
                            ).environmentObject(self.GameList)
                        ) {
                            CategoryItem(game: game)
                        }
                    }
                }
            }
            .frame(height: 185)
        }
        .padding(.bottom, 5)
    }
}

struct CategoryItem: View {
    var game: Game
    
    var body: some View {
        VStack(alignment: .leading) {
            Image(game.imageName)
                .renderingMode(.original)
                .resizable()
                .frame(width: 155, height: 155)
                .cornerRadius(5)
            Text(game.name)
                .foregroundColor(.primary)
                .font(.caption)
                .padding(.bottom, 5.0)
            
        }
        .padding(.leading, 10)
    }
}

struct CategoryRow_Previews: PreviewProvider {
    static var previews: some View {
        CategoryRow(CategoryName: game_data[0].category.rawValue,
                    items: Array(game_data.prefix(4)))
            .environmentObject(GameList())
    }
}
