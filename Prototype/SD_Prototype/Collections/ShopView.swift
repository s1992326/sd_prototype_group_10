//
//  ShopView.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/1.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct ShopView: View {
    var game: Game
    
    var body: some View {
        VStack {
            List {
                NavigationLink(destination: WebBrowser(the_url: website_data[3].the_url)) {
                    ShopRow(website: website_data[3], game: game)
                        .padding(.leading, -10)
                        .padding(.top)
                }
                NavigationLink(destination: WebBrowser(the_url: website_data[4].the_url)) {
                    ShopRow(website: website_data[4], game: game)
                        .padding(.leading, -10)
                        .padding(.top)
                }
                NavigationLink(destination: WebBrowser(the_url: website_data[5].the_url)) {
                    ShopRow(website: website_data[5], game: game)
                        .padding(.leading, -10)
                        .padding(.top)
                }
                NavigationLink(destination: WebBrowser(the_url: website_data[6].the_url)) {
                    ShopRow(website: website_data[6], game: game)
                        .padding(.leading, -10)
                        .padding(.top)
                }
            }
            .padding(.leading, -20)
            Spacer()
        }
        .navigationBarTitle(Text("Shopping"), displayMode: .inline)
    }
}

struct ShopView_Previews: PreviewProvider {
    static var previews: some View {
        ShopView(game: game_data[0])
    }
}
