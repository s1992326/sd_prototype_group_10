//
//  CollectionsView.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/3/31.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct CollectionsView: View {
    var categories: [String: [Game]] {
        Dictionary(
            grouping: game_data,
            by: {$0.category.rawValue}
        )
    }
    
    var body: some View {
        NavigationView {
            List {
                Image("CollectionsTitle")
                    .resizable()
                    .scaledToFill()
                    .frame(height: 200)
                    .clipped()
                    .listRowInsets(EdgeInsets())
                    .padding(.bottom, 15)
                    .padding(.horizontal, 20)
                
                ForEach(categories.keys.sorted(), id: \.self) { key in
                    CategoryRow(CategoryName: key, items: self.categories[key]!)
                        .environmentObject(GameList())
                }
                .listRowInsets(EdgeInsets())
                .padding(.horizontal, 20)
            }
            .navigationBarTitle("Collections", displayMode: .automatic)
            .padding(.horizontal, -20)
        }
    }
}

struct CollectionsView_Previews: PreviewProvider {
    static var previews: some View {
        CollectionsView()
    }
}
