//
//  ShopRow.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/3.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct ShopRow: View {
    var website: Website
    var game: Game
    
    var body: some View {
        VStack(spacing: 20) {
            HStack {
                Image(website.imageName)
                    .resizable()
                    .frame(width: 60, height: 60)
                    .cornerRadius(8)
                    .shadow(radius: 6)
                Text(website.name)
                    .font(.system(size: 24, weight: .bold))
                    .fontWeight(.bold)
                    .offset(x: 20)
                Spacer()
                Text("£ \(website.price[game.id - 1001], specifier: "%.2f")")
                    .font(.system(size: 22, weight: .bold))
                    .fontWeight(.bold)
                    .foregroundColor(Color.pink)
                    .multilineTextAlignment(.center)
                    .padding(.trailing, 30)
            }
            .padding(.leading, 18.0)
            .padding(.bottom, 10)
        }
        .padding(.leading, 20.0)
        .padding(.vertical, 10)
    }
}

extension LosslessStringConvertible {
    var string: String { .init(self) }
}

struct ShopRow_Previews: PreviewProvider {
    static var previews: some View {
        ShopRow(website: website_data[0], game: game_data[0])
    }
}
