//
//  GameDetails.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/4/1.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

final class GameList: ObservableObject {
    @Published var games = game_data
}

struct GameDetails: View {
    @EnvironmentObject var GameList: GameList
    
    var game: Game
    
    var GameIndex: Int {
        GameList.games.firstIndex(where: { $0.id == game.id })!
    }
    
    var post: Post {
        post_data[self.GameIndex]
    }
    
    var body: some View {
        VStack {
            ScrollView {
                Image(game.backgroundName)
                    .resizable()
                    .scaledToFill()
                    .frame(height: 250)
                    .clipped()
                    .listRowInsets(EdgeInsets())
                    .padding(.bottom, 10)
                
                
                VStack {
                    HStack {
                        Text(game.name)
                            .font(.system(size: 28))
                            .fontWeight(.bold)
                        
                        Button(action: {
                            self.GameList.games[self.GameIndex].isFavorite.toggle()
                        }) {
                            if self.GameList.games[self.GameIndex].isFavorite {
                                Image(systemName: "star.fill")
                                    .foregroundColor(Color.yellow)
                            } else {
                                Image(systemName: "star")
                                    .foregroundColor(Color.gray)
                            }
                        }
                        
                        Button(action: {
                            self.GameList.games[self.GameIndex].isOwned.toggle()
                        }) {
                            if self.GameList.games[self.GameIndex].isOwned {
                                Image(systemName: "checkmark.rectangle.fill")
                                    .foregroundColor(Color.green)
                            } else {
                                Image(systemName: "checkmark.rectangle")
                                    .foregroundColor(Color.gray)
                            }
                        }
                        
                        Spacer()
                        
                        NavigationLink(
                            destination: ShopView(game: game)
                        ) {
                            Image(systemName: "cart.badge.plus")
                                .resizable()
                                .scaledToFill()
                                .frame(width: 25, height: 25)
                        }
                        .offset(x: -70, y: 0)
                    }
                    .padding(.bottom, 20.0)
                    .offset(x: 35, y: 0)
                }
                
                VStack(alignment: .leading) {
                    Text("Description")
                        .font(.system(size: 24))
                        .fontWeight(.medium)
                        .padding(.leading, 10)
                        .padding(.top, 10)
                    
                    Text(game.description)
                        .lineLimit(nil)
                        .padding(.leading, 10)
                        .padding(.top, 15)
                }
                .offset(x: -55, y: 0)
                .padding(.bottom, 20.0)
                
                VStack(alignment: .leading) {
                    Text("Rules")
                        .font(.system(size: 24))
                        .fontWeight(.medium)
                        .padding(.leading, 10)
                        .padding(.top, 10)
                    
                    Text(game.rules)
                        .lineLimit(nil)
                        .padding(.leading, 10)
                        .padding(.top, 15)
                }
                .offset(x: -80, y: 0)
                .padding(.bottom, 10.0)
                
                VStack(alignment: .leading) {
                    ReviewDetails(post: post)
                        .padding(.all, 10)
                }
                
                Spacer()
                
            }
            .navigationBarTitle(Text(game.name), displayMode: .inline)
        }
    }
}

struct GameDetails_Previews: PreviewProvider {
    static var previews: some View {
        GameDetails(game: game_data[0])
            .environmentObject(GameList())
    }
}
