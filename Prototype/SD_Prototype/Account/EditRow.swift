//
//  EditRow.swift
//  SD_Prototype
//
//  Created by 笪雨嘉 on 2020/4/7.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct EditRow: View {
    var body: some View {
        Text("This is the editing page!!")
    }
}

struct EditRow_Previews: PreviewProvider {
    static var previews: some View {
        EditRow()
    }
}
