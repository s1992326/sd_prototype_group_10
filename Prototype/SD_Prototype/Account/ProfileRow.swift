//
//  ProfileRow.swift
//  SD_Prototype
//
//  Created by 笪雨嘉 on 2020/4/7.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct ProfileRow: View {
    var item: String
    var content: String
    var body: some View {
        HStack{
            Text(item)
                .font(.system(size: 24))
            Spacer()
            Text(content)
                .font(.system(size: 24))
        }
        .padding()
    }
}

struct ProfileRow_Previews: PreviewProvider {
    static var previews: some View {
        ProfileRow(item: "Username", content: "Test_player")
    }
}
