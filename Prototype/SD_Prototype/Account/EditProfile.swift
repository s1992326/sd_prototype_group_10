//
//  EditProfile.swift
//  SD_Prototype
//
//  Created by 笪雨嘉 on 2020/4/4.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct EditProfile: View {
    var player: Player
    
    var body: some View {
            VStack{
                Image(player.backgroundImage)
                    .resizable()
                    .scaledToFill()
                    .frame(height: 300)
                    .clipped()
                    .listRowInsets(EdgeInsets())
                    .padding(.bottom, -160)
                    .edgesIgnoringSafeArea(.top)
                
                List{
                    NavigationLink(destination: EditRow()){
                        HStack{
                              Text("Icon")
                                  .font(.system(size: 24))
                               Spacer()
                               Image(player.imageName)
                               .resizable()
                               .clipShape(Circle())
                               .frame(width: 80, height: 80)
                           }
                           .padding()
                    }
                   
                    
                    NavigationLink(destination: EditRow()){
                        ProfileRow(item: "Description", content: "")
                    }
                    
                    NavigationLink(destination: EditRow()){
                        ProfileRow(item: "Username", content: "player1")
                    }
                    
                    NavigationLink(destination: EditRow()){
                       ProfileRow(item: "Password", content: "")
                    }
                                       
                    
                    NavigationLink(destination: EditRow()){
                        ProfileRow(item: "Gender", content: "Male")
                    }
                    
                    NavigationLink(destination: EditRow()){
                        ProfileRow(item: "Date of Birth", content: "1997/01/01")
                    }
                    
                    NavigationLink(destination: EditRow()){
                        ProfileRow(item: "Email", content: "sample@gmail.com")
                    }
                    
                    NavigationLink(destination: EditRow()){
                        ProfileRow(item: "Place of living", content: "Edinburgh")
                    }
                    
                   
                }
                .padding(.horizontal, 45)
                
               
                Spacer()
                
                
                navigateButton(destinationView: Login(), title: "Logout")
                .padding(.horizontal, 40)
                
            }
        
    }
}

struct EditProfile_Previews: PreviewProvider {
    static var previews: some View {
        EditProfile(player: player_data[0])
    }
}
