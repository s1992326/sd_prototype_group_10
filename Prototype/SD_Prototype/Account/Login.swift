//
//  Login.swift
//  SD_Prototype
//
//  Created by 笪雨嘉 on 2020/4/4.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct Login: View {
    
    @State private var input_username: String = ""
    @State private var input_pwd: String = ""
    
    var body: some View {
        
        VStack{
            Spacer()
            VStack{
                
                Text("Login")
                    .font(.system(size: 40, weight: .bold))
                
                HStack {
                  Text("Username")
                   .font(.system(size: 28, weight: .bold))
                    .padding(.trailing)
                   
                  VStack {
                          
                       TextField("", text: $input_username)
                      .padding()
                      .frame(width: 220, height: 48)
                      .overlay(
                          RoundedRectangle(cornerRadius: 12)
                              .stroke(Color.white, lineWidth: 2)
                              .shadow(radius: 8)
                      )
                  }
                  .offset(x: -2, y: 0)
                }
                .padding()
                
                HStack {
                  Text("Password")
                   .font(.system(size: 28, weight: .bold))
                    .padding(.trailing)
                   
                  VStack {
                        
                       TextField("", text: $input_pwd)
                      .padding()
                      .frame(width: 220, height: 48)
                      .overlay(
                          RoundedRectangle(cornerRadius: 12)
                            .stroke(Color.white, lineWidth: 2)
                              .shadow(radius: 8)
                      )
                  }
                  .offset(x: -2, y: 0)
                }
                .padding()
                
                 navigateButton(destinationView: AccountView(player: player_data[0]), title: "Login")
               
           }
           .padding(.top, 30)
           .padding(.bottom, 30)
            .background(Color.white.opacity(0.5))
           .offset(y: 5)
            Spacer()
        }
        .background(
            Image("bg")
                .resizable()
                .scaledToFill()
                .edgesIgnoringSafeArea(.all)
        )
    }
}

struct Login_Previews: PreviewProvider {
    static var previews: some View {
        Login()
    }
}
