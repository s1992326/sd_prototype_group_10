//
//  AccountView.swift
//  SD_Prototype
//
//  Created by 吴昊琦 on 2020/3/31.
//  Copyright © 2020 Group_10. All rights reserved.
//

import SwiftUI

struct AccountView: View {
    var player: Player
    
    var body: some View {
        NavigationView {
            VStack {
                ScrollView{
                    Image(player.backgroundImage)
                        .resizable()
                        .scaledToFill()
                        .frame(height: 300)
                        .clipped()
                        .listRowInsets(EdgeInsets())
                        .padding(.bottom, -160)
                        .edgesIgnoringSafeArea(.top)
                    
                    NavigationLink(destination: EditProfile(player: player)){
                        CircleImage(image: Image(player.imageName).resizable())
                                               .padding(.bottom)
                    }
                    .buttonStyle(PlainButtonStyle())
                   
                    
                    Text(player.username)
                        .font(.largeTitle)
                        .fontWeight(.heavy)
                        .padding(.bottom)
                    
                    HStack {
                        VStack(alignment: .leading) {
                            Text("Description")
                                .font(.system(size: 24))
                                .fontWeight(.medium)
                                .multilineTextAlignment(.leading)
                                .padding(.leading, 80)
                                .padding(.top, 10)
                            
                            Text("This is the description of this player.")
                                .lineLimit(nil)
                                .padding(.leading, 80)
                                .padding(.top, 15)
                        }
                        Spacer()
                    }
                    .padding(.bottom, 20)
                    
                    Divider()
                    
                    Group{
                        VStack {
                            Text("Favorites")
                                .font(.system(size: 24))
                                .fontWeight(.medium)
                                .multilineTextAlignment(.leading)
                                .padding(.top, 10)
                                .offset(x: -135)
                            
                            GameCollection(gamelist: player.favoriteGame)
                                .environmentObject(GameList())
                                .padding(.leading, 67)
                        }
                        
                        Divider()
                        
                        VStack {
                            Text("Owned")
                                .font(.system(size: 24))
                                .fontWeight(.medium)
                                .multilineTextAlignment(.leading)
                                .padding(.top, 10)
                                .offset(x: -145)
                            
                            GameCollection(gamelist: player.ownedGame)
                                .environmentObject(GameList())
                                .padding(.leading, 67)
                                .padding(.bottom, 20)
                        }
                        
                        Divider()
                        
                        VStack {
                           Text("Clubs")
                               .font(.system(size: 24))
                               .fontWeight(.medium)
                               .multilineTextAlignment(.leading)
                               .padding(.top, 10)
                               .offset(x: -145)
                           
                            
                            ClubRow(club: club_data[0])
                            .padding(.leading, 67)
                            ClubRow(club: club_data[1])
                            .padding(.leading, 67)
                            
                            
                        }
                           
                        Divider()
                        
                        VStack {
                            Text("Location")
                                .font(.system(size: 24))
                                .fontWeight(.medium)
                                .multilineTextAlignment(.leading)
                                .padding(.top, 10)
                                .padding(.bottom, 10)
                                .offset(x: -135)
                            
                            MapView(coordinate: player.locationCoordinate)
                                .frame(height: 320)
                        }
                    }
                    
                   
                }
                                   
                
                 
            }
            .edgesIgnoringSafeArea(.top)
            .navigationBarTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
        }
    }
}

struct AccountView_Previews: PreviewProvider {
    static var previews: some View {
        AccountView(player: player_data[0])
    }
}


