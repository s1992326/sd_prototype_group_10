# Prototype instruction

The prototype is implemented on iOS system, so a MacBook and the software Xcode are required to run the application. Although this is a mobile app, it is not necessary to have an iPhone since Xcode is able to run a iPhone simulator. 

1. Search and install Xcode from App Store

    ![image](/Prototype/image/icon.png)

2. Clone the repository to local machine

3. Open Xcode,  click 'Open' in 'File' 

    ![image](/Prototype/image/open.png)

4. Open the project by the Prototype/SD_prototype.xcodeproj

    ![image](/Prototype/image/project.png)

5. Find 'Product' in the menu and click 'Run'

    ![image](/Prototype/image/Run.png)

6. iPhone 11 Pro Max simulator pops up. Click each component to see the prototype.

    ![image](/Prototype/image/final.png)


Caution: You may find Xcode reports an error about project signing or identifier, this is due to the lack of Apple ID registration. Please follow the following steps to resolve it:

1.	Click the project file, and turn to “Signing & Capabilities” page

    ![image](/Prototype/image/projectsign.png)

2.	Make sure you have a Team, a Bundle Identifier and a Signing Certificate, the Bundle Identifier can be different from the one in the figure

    ![image](/Prototype/image/identifier.png)

3.	If not, log in your Apple ID, and choose your Apple ID as the Team and Signing Certificate



**If there is any problem on viewing the prototype, please contact s1992326@ed.ac.uk . **





